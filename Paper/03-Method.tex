\section{Reference-aware Network}

Given a background in the form of free text $K=(k_{1}$, $k_2$, \ldots, $k_t$, \ldots, $k_{L_{K}})$ with $L_{K}$ tokens and a current conversational context $C_{\tau}=(\ldots$, $X_{\tau-3}$, $X_{\tau-2}$, $X_{\tau-1})$, the task of \ac{BBC} is to generate a response $X_{\tau}$ at $\tau$.
Each $X_{\tau}$ contains a sequence of $L_{X_\tau}$ units, i.e., $X_{\tau}=(x^\tau_{1},x^\tau_{2},\ldots,x^\tau_{t},\ldots,x^\tau_{{L_{X_\tau}}})$, where $x^\tau_{t}$, the unit at timestamp $t$, could be a token $\{x^\tau_{t,i}\}^1_{i=1}$ or a semantic unit $\{x^\tau_{t,i}\}^n_{i=1}$ containing $n$ tokens.

\ac{RefNet} consists of four modules: background encoder, context encoder, decoding switcher, and hybrid decoder; see Fig.~\ref{figure2}.
Background and context encoders encode the given background $K$ and context $C_{\tau}$ into latent representations $\mathbf{H}^k$ and $\mathbf{H}^c_\tau$, respectively.
$\mathbf{H}^k$ and $\mathbf{H}^c_\tau$ go through a matching layer to get a context-aware background representation $\mathbf{H}^m$.
At each decoding step, the decoding switcher predicts the probabilities of executing the \emph{reference decoding} or \emph{generation decoding}.
The hybrid decoder takes $\mathbf{H}^c_\tau$, $\mathbf{H}^m$ and the embedding of the previous token as input and computes the probability of selecting a semantic unit from the background (\emph{reference decoding}) or generating a token (\emph{generation decoding}) based on the decision made by the decoding switcher.
Next, we introduce the separate modules.

\subsection{Background and context encoders}
We use a bi-directional RNN \citep{schuster1997bidirectional} with GRU \citep{cho2014learning} to convert the context and background sequences into two hidden state sequences $\mathbf{H}^c_\tau=(\mathbf{h}_1^c$, $\mathbf{h}_2^c$, \ldots, $\mathbf{h}_{L_{C_\tau}}^c)$ and $\mathbf{H}^k=(\mathbf{h}_1^k$, $\mathbf{h}_2^k$, \ldots, $\mathbf{h}_{L_K}^k)$:
\begin{equation}
\setlength{\abovedisplayskip}{5pt}
\setlength{\belowdisplayskip}{5pt}
\label{encoder}
\begin{split}
\mathbf{h}_t^c={}&\mathbf{BiGRU}_c(\mathbf{h}_{t-1}^c, \mathbf{e}({x_t})),\\ \mathbf{h}_t^k={}&\mathbf{BiGRU}_k(\mathbf{h}_{t-1}^k, \mathbf{e}({k_t})),
\end{split}
\end{equation}
where $\mathbf{h}_t^c$ or $\mathbf{h}_t^k$ correspond to a token in the context or background, respectively, and $\mathbf{e}(x_t)$ and $\mathbf{e}(k_t)$ are the embedding vectors, respectively.
We concatenate the responses in the context, $L_{C_\tau}$ is the number of all tokens in the context, and we do not consider the segmentation of semantic units during encoding, i.e., each $x^\tau_{t}$ is a token $\{x^\tau_{t,i}\}^1_{i=1}$. 

Further, we use a matching layer \cite{wang2017machine,wang2017gated} to get the context-aware background representation $\mathbf{H}^m=(\mathbf{h}_1^m$, $\mathbf{h}_2^m$, \ldots, $\mathbf{h}_{L_K}^m)$:
\begin{equation}
\setlength{\abovedisplayskip}{5pt}
\setlength{\belowdisplayskip}{5pt}
\label{context-aware encoder}
\begin{split}
\mathbf{h}_t^m={}&\mathbf{BiGRU}_m(\mathbf{h}_{t-1}^m,[\mathbf{h}_t^k;\mathbf{c}_{t}^{kc}]),
\end{split}
\end{equation}
where $\mathbf{c}_{t}^{kc}$ is calculated using an attention mechanism \citep{attention2015} with $\mathbf{h}_t^k$ attentively reading $\mathbf{H}^c_\tau$: 
\begin{equation}
\setlength{\abovedisplayskip}{5pt}
\setlength{\belowdisplayskip}{5pt}
\label{ba}
\begin{split}
s^{kc}_{t,j}={}&\mathbf{v}_{kc}^\mathrm{T}\tanh(\mathbf{W}_{kc}\mathbf{h}_j^c +\mathbf{U}_{kc}\mathbf{h}^{k}_{t} + \mathbf{b}_{kc}), \\
\alpha^{kc}_{t,i}={}&\frac{\exp(s^{kc}_{t,i})}{\sum_{j=1}^{L_{C_\tau}} \exp(s^{kc}_{t,j})},\ 
\mathbf{c}_{t}^{kc}={}\sum_{i=1}^{L_{C_\tau}}\alpha^{kc}_{t,i}\mathbf{h}_i^c,\\
\end{split}
\end{equation}
where $\mathbf{W}_{kc}$, $\mathbf{U}_{kc}$, $\mathbf{v}_{kc}$ and $\mathbf{b}_{kc}$ are parameters.

\subsection{Hybrid decoder}
During training, we know that the next $x^\tau_{t}$ to be generated is a token $\{x^\tau_{t,i}\}^1_{i=1}$ or a semantic unit $\{x^\tau_{t,i}\}^n_{i=1}$.
If $x^\tau_{t}=\{x^\tau_{t,i}\}^n_{i=1}$, then $x^\tau_{t}$ is generated in \emph{reference decoding} mode with the probability modeled as follows:
\begin{equation}
\setlength{\abovedisplayskip}{5pt}
\setlength{\belowdisplayskip}{5pt}
\label{r_dec}
P(x^\tau_{t}|x^\tau_{<t}, C_\tau, K)=P(r)P(x^\tau_{t}\mid r),
\end{equation}
where $P(r)$ is the \emph{reference decoding} probability (see \S\ref{switcher}); $P(x^\tau_{t}\mid r)$ is the probability of generating $x^\tau_{t}$ under the \emph{reference decoding} $r$. % (see \S\ref{rd}).
If $x^\tau_{t}=\{x^\tau_{t,i}\}^1_{i=1}$, then $x^\tau_{t}$ is generated in \emph{generation decoding} mode with the probability modeled as:
\begin{equation}
\setlength{\abovedisplayskip}{5pt}
\setlength{\belowdisplayskip}{5pt}
\label{g_dec}
\begin{split}
&P(x^\tau_{t}| x^\tau_{<t}, C_\tau, K)=\\
&\quad P(g_p)P(x^\tau_{t}\mid g_p)+P(g_c)P(x^\tau_{t}\mid g_c),
\end{split}
\end{equation}
where $P(g)=P(g_p)+P(g_c)$ is the \emph{generation decoding} probability; $P(g_p)$ is the \emph{predicting generation decoding} probability (see \S\ref{switcher}) and $P(g_c)$ is the \emph{copying generation decoding} probability (see \S\ref{switcher}).
$P(x^\tau_{t}\mid g_p)$ and $P(x^\tau_{t}\mid g_c)$ are the probabilities of generating $x^\tau_{t}$ under $g_p$ and $g_c$, respectively. 


\subsubsection{Reference decoding.}
\label{rd}
Within \emph{reference decoding}, the probability of generating the semantic unit $\{x^\tau_{t,i}\}^n_{i=1}$ is evaluated as follows:
\begin{equation}
\setlength{\abovedisplayskip}{5pt}
\setlength{\belowdisplayskip}{5pt}
P(x^\tau_{t}=\{x^\tau_{t,i}\}^n_{i=1}|r)= \alpha^{r_1}_{t,start}\alpha^{r_2}_{t,start+n-1},
\end{equation}
where $\alpha^{r_1}_{t,start}$ and $\alpha^{r_2}_{start+n-1}$ are the probabilities of the start and end tokens of $\{x^\tau_{t,i}\}^n_{i=1}$ (from the background), respectively, which are estimated by two-hop pointers with respect to the context-aware background hidden state sequence $\mathbf{H}^m$.
The $\alpha^{r_1}_{t,start}$ is calculated by the first hop pointer, as shown in Eq.~\ref{ra1}:
\begin{equation}
\setlength{\abovedisplayskip}{5pt}
\setlength{\belowdisplayskip}{5pt}
\label{ra1}
\begin{split}
\mathbf{o}^{1}_t={}&\mathbf{W}_{o_1}[\mathbf{h}^{s}_{t};\mathbf{c}_{t}^{sc};\mathbf{c}_{t}^{sm}]+\mathbf{b}_{o_1}, \\
s^{r_1}_{t,j}{}={}&\mathbf{v}_{r}^\mathrm{T}\tanh(\mathbf{W}_r\mathbf{h}_j^{m} +\mathbf{U}_r\mathbf{o}^{1}_t + \mathbf{b}_r), \\
\alpha^{r_1}_{t,start}={}& \frac{\exp(s^{r_1}_{t,start})}{\sum_{j=1}^{L_{K}} \exp(s^{r_1}_{t,j})}, \\
\end{split}
\end{equation}
where $\mathbf{W}_{o_1}$, $\mathbf{W}_r$, $\mathbf{U}_r$, $\mathbf{v}_{r}$, $\mathbf{b}_{o_1}$ and $\mathbf{b}_r$ are parameters.
$\mathbf{h}^{s}_{t}$ is the decoding hidden state vector, the updating scheme of which will be detailed in \S\ref{su}.
$\mathbf{c}_{t}^{sc}$ and $\mathbf{c}_{t}^{sm}$ are calculated in a similar way like Eq.~\ref{ba} with $\mathbf{h}^{s}_{t}$ attentively reading $\mathbf{H}^c_\tau$ and $\mathbf{H}^m$, respectively.
The $\alpha^{r_2}_{t,start+n-1}$ is calculated by the second hop pointer, as shown in Eq.~\ref{ra2}:
\begin{equation}
\setlength{\abovedisplayskip}{5pt}
\setlength{\belowdisplayskip}{5pt}
\label{ra2}
\begin{split}
\mathbf{c}_{t}^{r}={}&\sum_{i=1}^{L_{K}}\alpha^{r_1}_{t,i}\mathbf{h}_i^m,\  \mathbf{o}^{2}_t{}=\mathbf{W}_{o_2}[\mathbf{o}^{1}_t;\mathbf{c}_{t}^{r}]+\mathbf{b}_{o_2},\\
s^{r_2}_{t,j}={}&\mathbf{v}_{r}^\mathrm{T}\tanh(\mathbf{W}_r\mathbf{h}_j^{m} +\mathbf{U}_r\mathbf{o}^{2}_t + \mathbf{b}_r), \\
\mbox{}\hspace*{-2mm}\alpha^{r_2}_{t,start+n-1}={}&\frac{\exp(s^{r_2}_{t,start+n-1})}{\sum_{j=1}^{L_{K}} \exp(s^{r_2}_{t,j})},\\
\end{split}
\end{equation}
where $\mathbf{W}_{o_2}$ and $\mathbf{b}_{o_2}$ are parameters.
\emph{Reference decoding} adopts soft pointers $\alpha^{r_1}_{t,start}$ and $\alpha^{r_2}_{start+n-1}$ to select semantic units, so it will not influence the automatic differentiation during training.

\subsubsection{Generation decoding.}
\label{gd}
Within \emph{predicting generation decoding}, the probability of predicting the token $x^\tau_{t}$ from the vocabulary is estimated as follows:
\begin{equation} 
\setlength{\abovedisplayskip}{5pt}
\setlength{\belowdisplayskip}{5pt}
\begin{split}
\mbox{}\hspace*{-2mm}P(x^\tau_{t}=\{x^\tau_{t,i}\}^1_{i=1} {\mid} g_p)\!=\!\mathrm{softmax}(\mathbf{W}_{g_p}\mathbf{o}^{1}_t+\mathbf{b}_{g_p}),\,
\end{split}
\end{equation}
where $\mathbf{W}_{g_p}$ and $\mathbf{b}_{g_p}$ are parameters and the vector $\mathbf{o}^{1}_t$ is the same one as in Eq.~\ref{ra1}. 

Within \emph{copying generation decoding}, the probability of copying the token $x^\tau_{t}$ from the background is estimated as follows:
\begin{equation}
\setlength{\abovedisplayskip}{5pt}
\setlength{\belowdisplayskip}{5pt}
\begin{split}
P(x^\tau_{t}=\{x^\tau_{t,i}\}^1_{i=1}\mid g_c)=\sum_{i:k_i=x^\tau_{t}}\alpha^{sm}_{t,i}, 
\end{split}
\end{equation}
where $\alpha^{sm}_{t,i}$ is the attention probability distribution on $\mathbf{H}^m$ produced by the same attention process with $\mathbf{c}_{t}^{sm}$ in Eq.~\ref{ra1}.
        
\subsection{Decoding switcher}
\label{switcher}
The decoding switching probabilities $P(r)$, $P(g_p)$ and $P(g_c)$ are estimated as follows:
\begin{equation}
\setlength{\abovedisplayskip}{5pt}
\setlength{\belowdisplayskip}{5pt}
\label{fusion vector}
\begin{split}
[P(r),P(g_p),P(g_c)]=\mathrm{softmax}(\mathbf{f}_t),
\end{split}
\end{equation}
where $\mathbf{f}_t$ is a fusion vector, which is computed through a linear transformation in Eq.~\ref{fv}:
\begin{equation}
\setlength{\abovedisplayskip}{5pt}
\setlength{\belowdisplayskip}{5pt}
\label{fv}
\begin{split}
\mathbf{f}_t=\mathbf{W}_f[\mathbf{h}^{s}_{t};\mathbf{c}_{t}^{sc};\mathbf{c}_{t}^{sm}]+\mathbf{b}_f,
\end{split}
\end{equation}
where $\mathbf{W}_f$ and $\mathbf{b}_f$ are parameters.
$\mathbf{h}^{s}_{t}$ is decoding states (see \S\ref{su}).

During testing, at each decoding step, we first compute $P(r)$ and $P(g)=P(g_p)+P(g_c)$.
If $P(r) \geq P(g)$, we use Eq.~\ref{r_dec} to generate a semantic unit, otherwise we use Eq.~\ref{g_dec} to generate a token.

\subsection{State updating}
\label{su}
The decoding state updating depends on whether the generated unit is a token or semantic unit. 
If $x^\tau_{t-1}$ is a token, then $\mathbf{h}^{s}_{t}=$
\begin{equation}
\setlength{\abovedisplayskip}{5pt}
\setlength{\belowdisplayskip}{5pt}
\label{tsu}
\begin{split}
&\mbox{}\hspace*{-1mm}\mathbf{GRU}(\mathbf{h}^{s}_{t-1},[\mathbf{e}(x^\tau_{t-1});\mathbf{c}_{t-1}^{sc};\mathbf{c}_{t-1}^{sm}]).\hspace*{-2mm}\mbox{}
\end{split}
\end{equation}
If $x^\tau_{t-1}$ is a span containing $n$ tokens, Eq.~\ref{tsu} will update $n$ times with one token as the input, and the last state will encode the full semantics of a span; see $\mathbf{h}^{s}_{t}$ to $\mathbf{h}^{s}_{t+1}$ in Fig.~\ref{figure2}. 

The decoding states are initialized using a linear layer with the last state of $\mathbf{H}^m$ and $\mathbf{H}^c_\tau$ as input:  
\begin{equation}
\setlength{\abovedisplayskip}{5pt}
\setlength{\belowdisplayskip}{5pt}
\label{states initialize}
\begin{split}
\mathbf{h}^{s}_{0}&=\text{ReLU}(\mathbf{W}_{hs}[\mathbf{h}_{L_K}^m;\mathbf{h}_{L_{C_\tau}}^c]+\mathbf{b}_{hs}),
\end{split}
\end{equation}
where $\mathbf{W}_{hs}$ and $\mathbf{b}_{hs}$ are parameters.
ReLU is the ReLU activation function.

\subsection{Training}
Our goal is to maximize the prediction probability of the target response given the context and background.
We have three objectives, namely generation loss, reference loss and switcher loss.

The \emph{generation loss} is defined as $\mathcal{L}_{g}(\theta)=$
\begin{equation}
\setlength{\abovedisplayskip}{5pt}
\setlength{\belowdisplayskip}{5pt}
\label{gen_loss}
\begin{split}
%\mathcal{L}_{g}(\theta)=
-\frac{1}{M}\sum_{\tau=1}^{M}\sum_{t=1}^{{L_{{X}_\tau}}}\log[P(x^\tau_{t}\mid x^\tau_{<t},C_{\tau},K)],
\end{split}
\end{equation}
where $\theta$ are all the parameters of \ac{RefNet}.
$M$ is the number of all training samples given a background $K$.
In $\mathcal{L}_{g}(\theta)$, each $x^\tau_{t}$ is a token $\{x^\tau_{t,i}\}^1_{i=1}$.

The \emph{reference loss} is defined as $\mathcal{L}_{r}(\theta)=$
\begin{equation}
\setlength{\abovedisplayskip}{5pt}
\setlength{\belowdisplayskip}{5pt}
\label{ref_loss}
\begin{split}
%\mathcal{L}_{r}(\theta)=\\
-\frac{1}{M}\sum_{\tau=1}^{M}\sum_{t=1}^{{L_{{X}_\tau}}}&I(x^\tau_{t})\cdot
\log[P(x^\tau_{t}\mid x^\tau_{<t},C_{\tau},K))],\hspace*{-2mm}\mbox{}
\end{split}
\end{equation}
where $I(x^\tau_{t})$ is an indicator function that equals 1 if $x^\tau_{t}=\{x^\tau_{t,i}\}^n_{i=1}$ and 0 otherwise. 

\ac{RefNet} introduces a decoding switcher to decide between \emph{reference decoding} and \emph{generation decoding}.
To better supervise this process we define \emph{switcher loss} $\mathcal{L}_{s}(\theta)=$
\begin{equation}
\setlength{\abovedisplayskip}{5pt}
\setlength{\belowdisplayskip}{5pt}
\label{switcher_loss} 
\begin{split}
%\mathcal{L}_{s}(\theta)=
\mbox{}\hspace*{-2mm}
{-}\frac{1}{M}\sum_{\tau=1}^{M}\sum_{t=1}^{{L_{{X}_\tau}}} I&(x^\tau_{t}) \log[P(r)]\!+\!
(1{-}I(x^\tau_{t}))\log[P(g)],
\hspace*{-1mm}\mbox{}
\end{split}
\end{equation}
where $I(x^\tau_{t})$ is also an indicator function, which is the same as in $\mathcal{L}_{r}(\theta)$.

The \emph{final loss} is a linear combination of the three loss functions just defined:
\begin{equation}
\setlength{\abovedisplayskip}{5pt}
\setlength{\belowdisplayskip}{5pt}
\label{loss}
\begin{split}
\mathcal{L}(\theta)=\mathcal{L}_{g}(\theta)+\mathcal{L}_{r}(\theta)+\mathcal{L}_{s}(\theta).
\end{split}
\end{equation}
All parameters of \ac{RefNet} as well as word embeddings are learned in an end-to-end back-propagation training paradigm.