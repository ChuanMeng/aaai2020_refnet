\section{Introduction}
Dialogue systems have attracted a lot of attention recently \cite{huang2019challenges}.
Sequence-to-sequence models \cite{sutskever2014sequence,shang2015neural} are an effective framework that is commonly adopted in existing studies.
However, a problem of sequence-to-sequence based methods is that they tend to generate generic and non-informative responses which provide deficient information~\cite{gao2019neural}.

\begin{figure}[t]
 \centering
 \includegraphics[width=0.47\textwidth]{figs/figure1.pdf}
 \caption{\acf{BBC}.}
 \label{f1}
\end{figure}

Previous research has proposed various methods to alleviate the issue, such as adjusting objective functions~\cite{li2016diversity,jiang-2019-improving}, incorporating external knowledge ~\cite{ghazvininejad2018knowledge,parthasarathi2018extending,dinan2018wizard}, etc.
Recently, \acfp{BBC} have been proposed for generating more informative responses that are grounded in some background information \cite{zhou2018dataset,moghe2018towards}.
As shown in Fig.~\ref{f1}, unlike previous conversational settings \citep{vinyals2015neural,serban2016building}, in a \ac{BBC} background material (e.g., a plot or review about a movie) is supplied to promote topic-specific conversations.

Existing methods for \acp{BBC} can be grouped into two categories, \emph{generation-based} methods (e.g., GTTP \cite{see2017get}) and \emph{extraction-based} methods (e.g., QANet \cite{yu2018qanet}).
Generation-based methods generate the response token by token, so they can generate natural and fluent responses, generally.
However, generation-based methods suffer from two issues.
First, they are relatively ineffective in leveraging background information.
For example, for the case in Fig.~\ref{f1}, S2SA does not leverage background information at all.
Second, they have difficulties locating the right semantic units in the background information.
Here, a \textit{semantic unit} is a span from the background information that expresses complete semantic meaning.
For example, in Fig.~\ref{f1}, the background contains many semantic units, e.g., ``\emph{mtv movie + tvawards 2004 best cameo}'' and ``\emph{scary movie 4}.''
GTTP uses the wrong semantic unit ``\emph{scary movie 4}'' to answer the question by ``human 2.''
Moreover, because generation-based methods generate the response one token at a time, they risk breaking a complete semantic unit, e.g., ``\emph{scary movie 4}'' is split by a comma in the response of GTTP in Fig.~\ref{f1}.
The reason is that generation-based methods lack a global perspective, i.e., each decoding step only focuses on a single (current) token and does not consider the tokens to be generated in the following steps.
Extraction-based methods extract a span from the background as their response and are relatively good at locating the right semantic unit.
But because of their extractive nature, they cannot generate natural conversational responses, see, e.g., the response of QANet in Fig.~\ref{f1}.

We propose a \textbf{Ref}erence-aware \textbf{Net}work (RefNet) to address above issues.
\ac{RefNet} consists of four modules: a \emph{background encoder}, a \emph{context encoder}, a \emph{decoding switcher}, and a \emph{hybrid decoder}.
The background encoder and context encoder encode the background and conversational context into representations, respectively.
Then, at each decoding step, the decoding switcher decides between \emph{reference decoding} and \emph{generation decoding}.
Based on the decision made by the decoding switcher, the hybrid decoder either selects a semantic unit from the background (\emph{reference decoding}) or generates a token otherwise (\emph{generation decoding}).
In the latter case, the decoding switcher further determines whether the hybrid decoder should predict a token from the vocabulary or copy one from the background.
Besides generating the response token by token, \ac{RefNet} also provides an alternative way to learn to select a semantic unit from the background directly.
Experiments on a \ac{BBC} dataset show that \ac{RefNet} significantly outperforms state-of-the-art methods in terms of both automatic and, especially, human evaluations.
% \ac{RefNet} obtains a much higher score in terms of humanness, which means that \ac{RefNet} can generate more human-like responses.

 Our contributions are as follows:
 \begin{itemize}[nosep]
 \item We propose a novel architecture, \ac{RefNet}, for \acp{BBC} by combing the advantages of extraction-based and generation-based methods. \ac{RefNet} can generate more informative and appropriate responses while retaining fluency. 
 \item We devise a decoding switcher and a hybrid decoder to adaptively coordinate between \emph{reference decoding} and \emph{generation decoding}.
 \item Experiments show that \ac{RefNet} outperforms state-of-the-art models by a large margin in terms of both automatic and human evaluations.
 \end{itemize}
